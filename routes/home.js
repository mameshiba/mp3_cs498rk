var secrets = require('../config/secrets');

module.exports = function (router) {

    var homeRoute = router.route('/');

    homeRoute.get(function (req, res) {
        var connectionString = secrets.token;
        res.json({ message: 'My connection string is ' + connectionString });
    });

    var User = require('../models/user.js');
    var Task = require('../models/task.js');

    // GET requests
    // users
    router.get('/users', function(req, res) {
        let query = User.find();
        let response = {message: "", data: {}};
        let status;
        //console.log('users');
        if (req.query !== {}) {
            //console.log(req.query);
            if ('count' in req.query && req.query.count === 'true') {
                response.data = 0;
            }
            if ('where' in req.query) {
                query.setQuery(JSON.parse(req.query.where));
            }
            if ('sort' in req.query) {
                query.sort(JSON.parse(req.query.sort));
            }
            if ('select' in req.query) {
                query.select(JSON.parse(req.query.select));
            }
            if ('skip' in req.query) {
                query.skip(parseInt(req.query.skip, 10));
            }
            if ('limit' in req.query) {
                query.limit(parseInt(req.query.limit, 10));
            }
        }
        query.exec(function(err, arr) {
            if (err) {
                status = 404;  
                response.message = "Sorry, we couldn't find any users that matched.";
                response.data = err;
                res.status(status).send(response);
            }   
            else {
                status = 200;
                response.message = "OK";
                if ('count' in req.query && req.query.count === 'true') {
                    response.data = arr.length;
                }
                else {
                    response.data = arr;
                }
                res.status(status).send(response);
            }
        });
    });

    router.get('/users/:id', function(req, res) {
        let query = User.findById(req.params.id);
        let response = {message: "", data: {}};
        let status;
        query.exec(function(err, doc) {
            if (err || doc === null) {
                status = 404;  
                response.message = "Sorry, the user does not exist.";
                response.data = err;
                res.status(status).send(response);
            }   
            else {
                status = 200;
                response.message = "OK";
                response.data = doc;
                res.status(status).send(response);
            }
        });
    });


    // tasks
    router.get('/tasks', function(req, res) {
        let query = Task.find();
        let response = {message: "", data: {}};
        let status;
        if (req.query !== {}) {
            //console.log(req.query);
            if ('count' in req.query && req.query.count === 'true') {
                response.data = 0;
            }
            if ('where' in req.query) {
                query.setQuery(JSON.parse(req.query.where));
            }
            if ('sort' in req.query) {
                query.sort(JSON.parse(req.query.sort));
            }
            if ('select' in req.query) {
                query.select(JSON.parse(req.query.select));
            }
            if ('skip' in req.query) {
                query.skip(parseInt(req.query.skip, 10));
            }
            if ('limit' in req.query) {
                query.limit(parseInt(req.query.limit, 10));
            }
        }
        query.exec(function(err, arr) {
            if (err) {
                status = 404;  
                response.message = "Sorry, we couldn't find any tasks that matched.";
                response.data = err;
                res.status(status).send(response);
            }   
            else {
                status = 200;
                response.message = "OK";
                if ('count' in req.query && req.query.count === 'true') {
                    response.data = arr.length;
                }
                else {
                    response.data = arr;
                }
                res.status(status).send(response);
            }
        });
    });

    router.get('/tasks/:id', function(req, res) {
        let query = Task.findById(req.params.id);
        let response = {message: "", data: {}};
        let status;
        query.exec(function(err, doc) {
            if (err || doc === null) {
                status = 404;  
                response.message = "Sorry, the task does not exist.";
                response.data = err;
                res.status(status).send(response);
            }   
            else {
                status = 200;
                response.message = "OK";
                response.data = doc;
                res.status(status).send(response);
            }
        });
    });


    // POST requests
    // users
    router.post('/users', async function(req, res) {
        let response = {message: "", data: {}};
        let status;
        //console.log('here');
        User.create(req.body, function(err, user) {
            if (err) {
                if (err.message.indexOf('duplicate key error') !== -1) {
                    status = 404;
                    response.message = "User must have unique email.";
                    response.data = err;
                }
                else if(err.message.indexOf('validation') !== -1) {
                    status = 404;
                    response.message = "Users cannot be created without a name or email.";
                    response.data = err;
                }
                else {
                    status = 500;
                    response.message = "Something went wrong with the server.";
                    response.data = err;
                }
                res.status(status).send(response);
            }
            else {
                //console.log('here');
                status = 201;
                response.message = "User has successfully been created.";
                response.data = user;
                for (let i = user.pendingTasks.length-1; i >= 0; i--) {
                    Task.findById(user.pendingTasks[i], async function(err, task) {
                        if (err === null && task !== null) {
                            if (!task.completed) {
                                if (String(task.assignedUser) !== "" && String(task.assignedUser) !== String(user._id)) {
                                    User.findById(task.assignedUser, async function(err, lastUser) {
                                        if (err === null && lastUser !== null) {
                                            const index = lastUser.pendingTasks.indexOf(task._id);
                                            if (index > -1) {
                                                lastUser.pendingTasks.splice(index, 1);
                                            }
                                            await lastUser.save();
                                        }
                                    });
                                }
                                task.assignedUser = user._id;
                                task.assignedUserName = user.name;
                                await task.save();
                            }
                            else {
                                user.pendingTasks.splice(i, 1);
                                user.save();
                            }
                        }
                    });
                }
                res.status(status).send(response);
            }
        });
        //console.log(status);
        //res.status(status).send(response);
    });

    // tasks
    router.post('/tasks', function(req, res) {
        let response = {message: "", data: {}};
        let status;
        Task.create(req.body, function(err, task) {
            if (err) {
                if (err.message.indexOf('validation') !== -1) {
                    status = 404;
                    response.message = "Tasks cannot be created without a name or a deadline.";
                    response.data = err;
                }
                else {
                    status = 500;
                    response.message = "Something went wrong with the server.";
                    response.data = err;
                }
                res.status(status).send(response);
            }
            else {
                status = 201;
                response.message = "Task has been successfully created.";
                response.data = task;
                if (String(task.assignedUser) !== "") {
                    User.findById(task.assignedUser, async function(err, user) {
                        if (err === null && user !== null) {
                            if (!task.completed) {
                                if (user.pendingTasks.indexOf(task._id) === -1) {
                                    user.pendingTasks.push(task._id);
                                }
                                await user.save();
                            }
                            else {
                                const index = user.pendingTasks.indexOf(task._id);
                                if (index > -1) {
                                    user.pendingTasks.splice(index, 1);
                                }
                                await user.save();
                            }
                        }
                    });
                }
                res.status(status).send(response);
            }
        });
    });

    // DELETE
    // user
    router.delete('/users/:id', function(req, res) {
        let response = {message: "", data: {}};
        let status;
        User.findByIdAndDelete(req.params.id, function(err, user) {
            if (err || user === null) {
                status = 404;
                response.message = "Sorry, the user does not exist.";
                response.data = err;
                res.status(status).send(response);
            }
            else {
                status = 200;
                response.message = "Successfully deleted user.";
                response.data = user;
                for (let i = 0; i < user.pendingTasks.length; i++) {
                    Task.findById(user.pendingTasks[i], async function(err, task) {
                        if (err === null && task !== null) {
                            task.assignedUser = "";
                            task.assignedUserName = "unassigned";
                            await task.save();
                        }
                    });
                }
                res.status(status).send(response);
            }
        });
    });
    
    // task
    router.delete('/tasks/:id', function(req, res) {
        let response = {message: "", data: {}};
        let status;
        Task.findByIdAndDelete(req.params.id, function(err, task) {
            if (err || task === null) {
                status = 404;
                response.message = "Sorry, the task does not exist."
                response.data = err;
                res.status(status).send(response);
            }
            else {
                status = 200;
                response.message = "Successfully deleted task.";
                response.data = task;
                if (String(task.assignedUser) !== "") {
                    User.findById(task.assignedUser, async function(err, user) {
                        if (err === null && user !== null) {
                            const index = user.pendingTasks.indexOf(task._id);
                            if (index > -1) {
                                user.pendingTasks.splice(index, 1);
                            }
                            await user.save();
                        }
                    });
                }
                res.status(status).send(response);
            }
        });
    });


    // PUT requests
    // user
    router.put('/users/:id', function(req, res) {
        let response = {message: "", data: {}};
        let status;
        User.findByIdAndDelete(req.params.id, function(err, user) {
            if (err) {
                status = 404;
                response.message = "Sorry, the user does not exist.";
                response.data = err;
                res.status(status).send(response);
            }
            else {
                bodyWithId = req.body;
                bodyWithId._id = req.params.id;
                User.create(bodyWithId, async function(err, newUser) {
                    if (err) {
                        if (err.message.indexOf('duplicate key error') !== -1) {
                            status = 404;
                            response.message = "User must have unique email.";
                            response.data = err;
                        }
                        else if(err.message.indexOf('validation') !== -1) {
                            status = 404;
                            response.message = "Users cannot be created without a name or email.";
                            response.data = err;
                        }
                        else {
                            status = 500;
                            response.message = "Something went wrong with the server.";
                            response.data = err;
                        }
                        let body = {};
                        body._id = user._id;
                        body.email = user.email;
                        body.name = user.name;
                        body.pendingTasks = user.pendingTasks;
                        User.create(body, async function(err2, originalUser) {
                            if (err2) {
                                response.data = err2;
                            }
                            res.status(status).send(response);
                        });
                    }
                    else {
                        status = 200;
                        response.message = "User has successfully been replaced.";
                        //console.log(user);
                        //console.log(newUser);
                        response.data = newUser;
                        for (let i = 0; i < user.pendingTasks.length; i++) {
                            if (!(newUser.pendingTasks.indexOf(user.pendingTasks[i]) > -1)) {
                                Task.findById(user.pendingTasks[i], function(err, task) {
                                    if (err === null && task !== null) {
                                        task.assignedUser = "";
                                        task.assignedUserName = "unassigned";
                                        task.save();
                                    }
                                });
                            }
                        }
                        
                        for (let i = newUser.pendingTasks.length-1; i >= 0; i--) {
                            Task.findById(newUser.pendingTasks[i], function(err, task) {
                                if (err === null && task !== null) {
                                    if (!task.completed) {
                                        //console.log(task.assignedUser);
                                        //console.log(newUser._id);
                                        
                                        if (String(task.assignedUser) !== "" && String(task.assignedUser) !== String(newUser._id)) {
                                            //console.log('here');
                                            //console.log(task.assignedUser);
                                            //console.log(newUser._id);
                                            User.findById(task.assignedUser, function(err, lastUser) {
                                                if (err === null && lastUser !== null) {
                                                    const index = lastUser.pendingTasks.indexOf(task._id);
                                                    if (index > -1) {
                                                        lastUser.pendingTasks.splice(index, 1);
                                                    }
                                                    lastUser.save();
                                                }
                                            });
                                        }
                                        task.assignedUser = newUser._id;
                                        task.assignedUserName = newUser.name;
                                        task.save();
                                    }
                                    else {
                                        newUser.pendingTasks.splice(i, 1);
                                        newUser.save();
                                    }
                                }
                            });
                        }
                        res.status(status).send(response);
                    }
                });
            }
        });
    });

    // task
    router.put('/tasks/:id', function(req, res) {
        let response = {message: "", data: {}};
        let status;
        Task.findByIdAndDelete(req.params.id, function(err, task) {
            if (err) {
                status = 404;
                response.message = "Sorry, the task does not exist."
                response.data = err;
                res.status(status).send(response);
            }
            else {
                bodyWithId = req.body;
                bodyWithId._id = req.params.id;
                Task.create(bodyWithId, function(err, newTask) {
                    if (err) {
                        if (err.message.indexOf('validation') !== -1) {
                            status = 404;
                            response.message = "Tasks cannot be created without a name or a deadline.";
                            response.data = err;
                        }
                        else {
                            status = 500;
                            response.message = "Something went wrong with the server.";
                            response.data = err;
                        }
                        let body = {}
                        body.name = task.name;
                        body.description = task.description;
                        body.deadline = task.deadline;
                        body.completed = task.completed;
                        body.assignedUser = task.assignedUser;
                        body.assignedUserName = task.assignedUserName;
                        Task.create(body, async function(err2, originalTask) {
                            if (err2) {
                                response.data = err2;
                            }
                            res.status(status).send(response);
                        });
                    }
                    else {
                        status = 200;
                        response.message = "Task has been successfully replaced.";
                        response.data = newTask;

                        if (String(task.assignedUser) !== "" && String(task.assignedUser) !== String(newTask.assignedUser)) {
                            User.findById(task.assignedUser, async function(err, user) {
                                if (err === null && user !== null) {
                                    const index = user.pendingTasks.indexOf(task._id);
                                    if (index > -1) {
                                        user.pendingTasks.splice(index, 1);
                                    }
                                    await user.save();
                                }
                            });
                        }
                        if (String(newTask.assignedUser) !== "") {
                            User.findById(newTask.assignedUser, async function(err, user) {
                                if (err === null && user !== null) {
                                    if (!newTask.completed) {
                                        if (user.pendingTasks.indexOf(newTask._id) === -1) {
                                            user.pendingTasks.push(newTask._id);
                                        }
                                        await user.save();
                                    }
                                    else {
                                        const index = user.pendingTasks.indexOf(newTask._id);
                                        if (index > -1) {
                                            user.pendingTasks.splice(index, 1);
                                        }
                                        await user.save();
                                    }
                                }
                            });
                        }
                        res.status(status).send(response);
                    }
                });
            }
        });
    });

    return router;
}
